export const isObject = obj => obj && typeof obj === 'object'
export const isArray = arr => Array.isArray(arr)
export const isStringWithText = str =>
  str && typeof str === 'string' && str.length > 0
