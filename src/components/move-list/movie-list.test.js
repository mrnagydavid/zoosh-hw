import { shallowMount } from '@vue/test-utils'
import { QExpansionItem } from 'quasar'
import MovieList from './movie-list.vue'

const TEST_CONFIG_STEP1 = {
  list: [
    {
      id: 'tt123456',
      title: 'Movie Title',
      actors: 'Actor #1, Actor #2',
      year: 2060,
      image: {
        url: 'image-url',
        width: 300,
        height: 200,
      },
      url: 'https://www.imdb.com',
    },
  ],
}

const TEST_CONFIG_STEP2 = {
  list: [
    {
      ...TEST_CONFIG_STEP1.list[0],
      details: 'Some details from Wikipedia',
      wikiUrl: 'https://en.wikipedia.org',
    },
  ],
}

describe('MovieList Component', () => {
  it('is a Vue component', () => {
    const component = shallowMount(MovieList, {
      propsData: {
        ...TEST_CONFIG_STEP2,
      },
    })
    expect(component.isVueInstance()).toEqual(true)
  })

  it('matches the snapshot', () => {
    const component1 = shallowMount(MovieList, {
      propsData: {
        ...TEST_CONFIG_STEP1,
      },
    })
    expect(component1.html()).toMatchSnapshot()

    const component2 = shallowMount(MovieList, {
      propsData: {
        ...TEST_CONFIG_STEP2,
      },
    })
    expect(component2.html()).toMatchSnapshot()
  })

  it('emits a @get-details event - if details are missing', () => {
    const component = shallowMount(MovieList, {
      propsData: {
        ...TEST_CONFIG_STEP1,
      },
    })
    const qItem = component.find(QExpansionItem)

    qItem.vm.$emit('before-show')

    expect(component.emitted('get-details')[0]).toEqual([
      TEST_CONFIG_STEP1.list[0].id,
    ])
  })

  it('does not emit a @get-details event - if details already exist', () => {
    const component = shallowMount(MovieList, {
      propsData: {
        ...TEST_CONFIG_STEP2,
      },
    })
    const qItem = component.find(QExpansionItem)

    qItem.vm.$emit('before-show')

    expect(component.emitted('get-details')).toEqual(undefined)
  })
})
