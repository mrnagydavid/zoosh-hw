import { shallowMount, mount } from '@vue/test-utils'
import { QBtn, QInput } from 'quasar'
import SearchInput from './search-input.vue'

describe('SearchInput Component', () => {
  it('is a Vue component', () => {
    const component = shallowMount(SearchInput)
    expect(component.isVueInstance()).toEqual(true)
  })

  it('matches the snapshot', () => {
    const component = shallowMount(SearchInput)
    expect(component.html()).toMatchSnapshot()
  })

  it('emits a @search event - button click', () => {
    const component = shallowMount(SearchInput)
    const qInput = component.find(QInput)
    const qBtn = component.find(QBtn)
    const QUERY = 'test-query'

    qInput.vm.$emit('input', QUERY)
    qBtn.vm.$emit('click')

    expect(component.emitted('search')[0]).toEqual([QUERY])
  })

  it('emits a @search event - ENTER', () => {
    const component = mount(SearchInput)
    const qInput = component.find(QInput)
    const QUERY = 'test-query'

    qInput.vm.$emit('input', QUERY)
    qInput.find('input').trigger('keyup.enter')

    expect(component.emitted('search')[0]).toEqual([QUERY])
  })

  it('does not emit an empty @search event', () => {
    const component = mount(SearchInput)
    const qInput = component.find(QInput)
    const qBtn = component.find(QBtn)

    qInput.find('input').trigger('keyup.enter')
    expect(component.emitted('search')).toBeUndefined()

    qBtn.vm.$emit('click')
    expect(component.emitted('search')).toBeUndefined()
  })
})
