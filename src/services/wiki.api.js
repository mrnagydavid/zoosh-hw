import jsonp from 'jsonp'
import { isObject, isArray } from 'util'

const SEARCH_CALLBACK_FN_NAME = 'handleWikiSearchJSONP'
const SUMMARY_CALLBACK_FN_NAME = 'handleWikiSummaryJSONP'
const SUMMARY_NR_OF_SENTENCES = 5

const strip = str => str.replace(/<[^>]*>/g, '')
const title2URL = title =>
  `https://en.wikipedia.org/wiki/${title.replace(/ /g, '_')}`

export const parseSuggestionsJSONP = json => {
  if (!isObject(json)) {
    return []
  }

  const { query } = json
  if (!isObject(query)) {
    return []
  }

  const { search } = query
  if (!isArray(search)) {
    return []
  }

  return search.map(({ snippet, title }) => ({
    snippet: strip(snippet),
    title,
    url: title2URL(title),
  }))
}

export const parseSummaryJSONP = json => {
  if (!isObject(json)) {
    return {}
  }

  const { query } = json
  if (!isObject(query)) {
    return {}
  }

  const { pages } = query
  if (!isObject(pages)) {
    return {}
  }

  if (pages['-1']) {
    return {}
  }

  const id = Object.keys(pages)[0]
  const { title, extract } = pages[id]

  return {
    title,
    extract: strip(extract),
    url: title2URL(title),
  }
}

export const WIKI_SEARCH_URL = (query, cb) => {
  const q_enc = encodeURIComponent(query)
  return `https://en.wikipedia.org/w/api.php?action=query&format=json&list=search&callback=${cb}&srsearch=${q_enc}`
}

export const WIKI_SUMMARY_URL = (title, sentences, cb) => {
  const t_enc = title.replace(/ /g, '_')
  return `https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exsentences=${sentences}&exlimit=1&callback=${cb}&titles=${t_enc}`
}

export function getWikiSuggestions(query) {
  return new Promise((resolve, reject) => {
    jsonp(
      WIKI_SEARCH_URL(query, SEARCH_CALLBACK_FN_NAME),
      { name: SEARCH_CALLBACK_FN_NAME },
      (err, data) => {
        if (err) {
          reject(err)
        } else {
          resolve(parseSuggestionsJSONP(data))
        }
      }
    )
  })
}

export function getWikiPageSummary(title) {
  return new Promise((resolve, reject) => {
    jsonp(
      WIKI_SUMMARY_URL(
        title,
        SUMMARY_NR_OF_SENTENCES,
        SUMMARY_CALLBACK_FN_NAME
      ),
      { name: SUMMARY_CALLBACK_FN_NAME },
      (err, data) => {
        if (err) {
          reject(err)
        } else {
          resolve(parseSummaryJSONP(data))
        }
      }
    )
  })
}
