import jsonp from 'jsonp'
import {
  WIKI_SEARCH_URL,
  WIKI_SUMMARY_URL,
  parseSuggestionsJSONP,
  parseSummaryJSONP,
  getWikiSuggestions,
  getWikiPageSummary,
} from './wiki.api'

jest.mock('jsonp')

describe('Wikipedia API', () => {
  describe('WIKI_SEARCH_URL', () => {
    it('transforms a query string into an Wikipedia Search URL', () => {
      const CALLBACK_FN = 'CALLBACK_FN'
      const QUERY = 'Terminator II'
      const EXPECT = 'Terminator%20II'
      expect(WIKI_SEARCH_URL(QUERY, CALLBACK_FN)).toEqual(
        `https://en.wikipedia.org/w/api.php?action=query&format=json&list=search&callback=${CALLBACK_FN}&srsearch=${EXPECT}`
      )
    })
  })

  describe('WIKI_SUMMARY_URL', () => {
    it('transforms a query string into an Wikipedia Summary URL', () => {
      const SENTENCES = 5
      const CALLBACK_FN = 'CALLBACK_FN'
      const QUERY = 'Terminator II'
      const EXPECT = 'Terminator_II'
      expect(WIKI_SUMMARY_URL(QUERY, SENTENCES, CALLBACK_FN)).toEqual(
        `https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exsentences=${SENTENCES}&exlimit=1&callback=${CALLBACK_FN}&titles=${EXPECT}`
      )
    })
  })

  describe('parseSuggestionsJSONP', () => {
    it('transforms the Wikipedia response into an application-standard result object', () => {
      const RESPONSE = {
        batchcomplete: '',
        continue: {
          sroffset: 10,
          continue: '-||',
        },
        query: {
          searchinfo: {
            totalhits: 207562,
          },
          search: [
            {
              ns: 0,
              title: 'Meaning',
              pageid: 18916,
              size: 1783,
              wordcount: 203,
              snippet:
                '<span class="searchmatch">Meaning</span> may refer to: <span class="searchmatch">Meaning</span> (existential), the worth of life in contemporary existentialism <span class="searchmatch">Meaning</span> (linguistics), <span class="searchmatch">meaning</span> which is communicated through',
              timestamp: '2019-08-17T10:38:05Z',
            },
          ],
        },
      }
      expect(parseSuggestionsJSONP(RESPONSE)).toMatchSnapshot()
    })

    it('fails silently and returns an empty array if the Wikipedia response is missing or malformed', () => {
      const BAD_RESPONSES = [undefined, [], '', 0, false, {}, { query: {} }]
      BAD_RESPONSES.forEach(r => expect(parseSuggestionsJSONP(r)).toEqual([]))
    })
  })

  describe('parseSummaryJSONP', () => {
    it('transforms the Wikipedia response into an application-standard result object', () => {
      const RESPONSE = {
        batchcomplete: '',
        warnings: {
          extracts: {
            '*':
              'The "exsentences" parameter may have unexpected results when used in HTML mode.\nHTML may be malformed and/or unbalanced and may omit inline images. Use at your own risk. Known problems are listed at https://www.mediawiki.org/wiki/Extension:TextExtracts#Caveats.',
          },
        },
        query: {
          normalized: [
            {
              from: 'Terminator_2:_Judgment_Day',
              to: 'Terminator 2: Judgment Day',
            },
          ],
          pages: {
            '34344124': {
              pageid: 34344124,
              ns: 0,
              title: 'Terminator 2: Judgment Day',
              extract:
                '<p><i><b>Terminator 2: Judgment Day</b></i> (also promoted as <i><b>T2</b></i>) is a 1991 American science fiction action film produced and directed by James Cameron, who also co-wrote it with William Wisher. The film stars Arnold Schwarzenegger, Linda Hamilton, Robert Patrick, and Edward Furlong as its principal cast. It is the sequel to the 1984 film <i>The Terminator</i>, as well as the second installment in the <i>Terminator</i> franchise. <i>Terminator&#160;2</i> follows Sarah Connor (Hamilton) and her ten-year-old son John (Furlong) as they are pursued by a new, more advanced Terminator: the liquid metal, shapeshifting T-1000 (Patrick), sent back in time to kill John Connor and prevent him from becoming the leader of the human resistance. A second, less advanced Terminator (Schwarzenegger) is also sent back in time to protect John.</p>',
            },
          },
        },
      }
      expect(parseSummaryJSONP(RESPONSE)).toMatchSnapshot()
    })

    it('fails silently and returns an empty array if the Wikipedia response is missing or malformed', () => {
      const BAD_RESPONSES = [
        undefined,
        [],
        '',
        0,
        false,
        {},
        { query: {} },
        { query: { pages: { '-1': {} } } },
      ]
      BAD_RESPONSES.forEach(r => expect(parseSummaryJSONP(r)).toEqual({}))
    })
  })

  describe('getWikiSuggestions', () => {
    it('retrieves suggestions from the Wikipedia JSONP API', async () => {
      const query = 'Meaning'
      const RESPONSE = {
        batchcomplete: '',
        continue: {
          sroffset: 10,
          continue: '-||',
        },
        query: {
          searchinfo: {
            totalhits: 207562,
          },
          search: [
            {
              ns: 0,
              title: 'Meaning',
              pageid: 18916,
              size: 1783,
              wordcount: 203,
              snippet:
                '<span class="searchmatch">Meaning</span> may refer to: <span class="searchmatch">Meaning</span> (existential), the worth of life in contemporary existentialism <span class="searchmatch">Meaning</span> (linguistics), <span class="searchmatch">meaning</span> which is communicated through',
              timestamp: '2019-08-17T10:38:05Z',
            },
          ],
        },
      }
      jsonp.mockImplementation((_a, _b, c) => {
        c(undefined, RESPONSE)
      })
      const result = await getWikiSuggestions(query)
      expect(result).toMatchSnapshot()
    })
  })

  describe('getWikiPageSummary', () => {
    it('retrieves suggestions from the Wikipedia JSONP API', async () => {
      const query = 'Terminator 2: Judgement Day'
      const RESPONSE = {
        batchcomplete: '',
        warnings: {
          extracts: {
            '*':
              'The "exsentences" parameter may have unexpected results when used in HTML mode.\nHTML may be malformed and/or unbalanced and may omit inline images. Use at your own risk. Known problems are listed at https://www.mediawiki.org/wiki/Extension:TextExtracts#Caveats.',
          },
        },
        query: {
          normalized: [
            {
              from: 'Terminator_2:_Judgment_Day',
              to: 'Terminator 2: Judgment Day',
            },
          ],
          pages: {
            '34344124': {
              pageid: 34344124,
              ns: 0,
              title: 'Terminator 2: Judgment Day',
              extract:
                '<p><i><b>Terminator 2: Judgment Day</b></i> (also promoted as <i><b>T2</b></i>) is a 1991 American science fiction action film produced and directed by James Cameron, who also co-wrote it with William Wisher. The film stars Arnold Schwarzenegger, Linda Hamilton, Robert Patrick, and Edward Furlong as its principal cast. It is the sequel to the 1984 film <i>The Terminator</i>, as well as the second installment in the <i>Terminator</i> franchise. <i>Terminator&#160;2</i> follows Sarah Connor (Hamilton) and her ten-year-old son John (Furlong) as they are pursued by a new, more advanced Terminator: the liquid metal, shapeshifting T-1000 (Patrick), sent back in time to kill John Connor and prevent him from becoming the leader of the human resistance. A second, less advanced Terminator (Schwarzenegger) is also sent back in time to protect John.</p>',
            },
          },
        },
      }
      jsonp.mockImplementation((_a, _b, c) => {
        c(undefined, RESPONSE)
      })
      const result = await getWikiPageSummary(query)
      expect(result).toMatchSnapshot()
    })
  })
})
