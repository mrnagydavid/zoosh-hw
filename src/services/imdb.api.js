import jsonp from 'jsonp'
import { isObject, isArray } from 'util'

export const IMDB_SUGGESTION_URL = query => {
  const q_enc = encodeURIComponent(query)
  const firstLetter = query.charAt(0).toLowerCase()
  return `https://v2.sg.media-imdb.com/suggests/${firstLetter}/${q_enc}.json`
}

export const IMDB_JSONP_CALLBACK = query => `imdb$${query.replace(/ /g, '_')}`

export const IMDB_URL = id => {
  const type = id.substring(0, 2)
  let route = ''
  if (type === 'tt') {
    route = 'title'
  } else if (type === 'nm') {
    route = 'name'
  } else {
    return
  }
  return `https://www.imdb.com/${route}/${id}`
}

export const IMDB_SEARCH_URL = title => `https://www.imdb.com/find?q=${title}`

export const parseIMDBJSON = json => {
  if (!isObject(json)) {
    return []
  }

  const { d: data } = json
  if (!isArray(data)) {
    return []
  }

  return data.map(
    ({
      l: title,
      id,
      s: actors,
      y: year,
      i: [img_url, img_w, img_h] = [],
    }) => ({
      id,
      title,
      actors,
      year,
      image: {
        url: img_url,
        width: img_w,
        height: img_h,
      },
      url: IMDB_URL(id) || IMDB_SEARCH_URL(title),
    })
  )
}

export function getIMDBSuggestions(query) {
  return new Promise((resolve, reject) => {
    jsonp(
      IMDB_SUGGESTION_URL(query),
      {
        name: IMDB_JSONP_CALLBACK(query),
      },
      (err, data) => {
        if (err) {
          reject(err)
        } else {
          resolve(parseIMDBJSON(data))
        }
      }
    )
  })
}
