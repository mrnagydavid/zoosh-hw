import jsonp from 'jsonp'
import {
  IMDB_SUGGESTION_URL,
  IMDB_JSONP_CALLBACK,
  IMDB_URL,
  IMDB_SEARCH_URL,
  parseIMDBJSON,
  getIMDBSuggestions,
} from './imdb.api'

jest.mock('jsonp')

describe('IMDB API', () => {
  describe('IMDB_SUGGESTION_URL', () => {
    it('transforms a query string into an IMDB JSONP URL', () => {
      expect(IMDB_SUGGESTION_URL('Terminator II')).toEqual(
        'https://v2.sg.media-imdb.com/suggests/t/Terminator%20II.json'
      )
    })
  })

  describe('IMDB_JSONP_CALLBACK', () => {
    it('creates the function name the JSONP response is going to call', () => {
      expect(IMDB_JSONP_CALLBACK('Terminator II')).toEqual('imdb$Terminator_II')
      expect(IMDB_JSONP_CALLBACK('A b c d')).toEqual('imdb$A_b_c_d')
    })
  })

  describe('IMDB_URL', () => {
    it('creates an IMDB url from the IMDB content id', () => {
      const tests = [
        ['tt12345', 'https://www.imdb.com/title/tt12345'],
        ['nm12345', 'https://www.imdb.com/name/nm12345'],
        ['as12345', undefined],
      ]
      tests.forEach(test => expect(IMDB_URL(test[0])).toEqual(test[1]))
    })
  })

  describe('IMDB_SEARCH_URL', () => {
    it('creates an IMDB search url from the IMDB content title', () => {
      const test = [
        'Not found movie',
        'https://www.imdb.com/find?q=Not found movie',
      ]
      expect(IMDB_SEARCH_URL(test[0])).toEqual(test[1])
    })
  })

  describe('parseIMDBJSON', () => {
    it('transforms the IMDB response into an application-standard result object', () => {
      const RESPONSE = {
        v: 1,
        q: 'terminator_ii',
        d: [
          {
            l: 'Shocking Dark',
            id: 'tt0098321',
            s: 'Christopher Ahrens, Haven Tyler',
            y: 1989,
            q: 'feature',
            i: [
              'https://m.media-amazon.com/images/M/MV5BZjQxODA1NTgtMDg2Mi00N2E4LWJlZTUtYmQyMWE0Mzk1ZTAwXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_.jpg',
              1063,
              1500,
            ],
          },
        ],
      }
      expect(parseIMDBJSON(RESPONSE)).toMatchSnapshot()
    })

    it('fails silently and returns an empty array if the IMDB response is missing or malformed', () => {
      const BAD_RESPONSES = [undefined, [], '', 0, false, {}]
      BAD_RESPONSES.forEach(r => expect(parseIMDBJSON(r)).toEqual([]))
    })
  })

  describe('getIMDBSuggestions', () => {
    it('retrieves suggestions from the IMDB JSONP API', async () => {
      const query = 'Terminator II'
      const RESPONSE = {
        v: 1,
        q: 'terminator_ii',
        d: [
          {
            l: 'Shocking Dark',
            id: 'tt0098321',
            s: 'Christopher Ahrens, Haven Tyler',
            y: 1989,
            q: 'feature',
            i: [
              'https://m.media-amazon.com/images/M/MV5BZjQxODA1NTgtMDg2Mi00N2E4LWJlZTUtYmQyMWE0Mzk1ZTAwXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_.jpg',
              1063,
              1500,
            ],
          },
        ],
      }
      jsonp.mockImplementation((_a, _b, c) => {
        c(undefined, RESPONSE)
      })
      const result = await getIMDBSuggestions(query)
      expect(result).toMatchSnapshot()
    })
  })
})
