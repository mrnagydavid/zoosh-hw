export function hitMap(state) {
  return state.hits.reduce((acc, hit) => ({ ...acc, [hit.id]: hit }), {})
}
