export function saveHits(state, hits) {
  state.hits = hits
}

export function clearHits(state) {
  state.hits = []
}

export function saveError(state, message) {
  state.error = true
  state.errorMessage = message
}

export function clearError(state) {
  state.error = false
  state.errorMessage = ''
}

export function saveDetails(state, { id, details, url }) {
  /// TODO: skip the search by receiving index and item from action in payload
  const index = state.hits.findIndex(hit => hit.id === id)
  if (index === -1) {
    return
  }

  const item = state.hits[index]
  const newItem = {
    ...item,
    details,
    wikiUrl: url,
  }

  state.hits.splice(index, 1, newItem)
}
