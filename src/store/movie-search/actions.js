import { getIMDBSuggestions } from '../../services/imdb.api'
import { isStringWithText } from '../../utils/utils'
import { getWikiSuggestions, getWikiPageSummary } from '../../services/wiki.api'

export async function search({ commit }, payload) {
  try {
    if (!isStringWithText(payload)) {
      commit('saveError', 'Please, type something into the search bar!')
      return
    }

    commit('clearHits')
    commit('clearError')

    const suggestions = await getIMDBSuggestions(payload)
    commit('saveHits', suggestions)
  } catch (error) {
    commit('saveError', error.message || error)
  }
}

export async function searchDetails({ commit, getters }, payload) {
  try {
    if (!isStringWithText(payload)) {
      /// TODO: handle error differently
      commit('saveDetails', {
        id: payload,
        details: 'No information',
        url: '',
      })
      return
    }

    const { title } = getters.hitMap[payload]

    const list = await getWikiSuggestions(title)

    if (list.length === 0) {
      commit('saveDetails', {
        id: payload,
        details: 'No information',
        url: '',
      })
      return
    }

    const { title: wikiTitle } = list[0]
    const { url, extract } = await getWikiPageSummary(wikiTitle)

    const details = {
      id: payload,
      url,
      details: extract,
    }

    commit('saveDetails', details)
  } catch (error) {
    /// TODO: handle error differently
    commit('saveDetails', {
      id: payload,
      details: 'No information',
      url: '',
    })
  }
}
