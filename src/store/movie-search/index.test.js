import store from './index'
import * as IMDB_API from '../../services/imdb.api'
import * as WIKI_API from '../../services/wiki.api'

jest.mock('../../services/imdb.api')
jest.mock('../../services/wiki.api')

describe('movie-search Vuex Module', () => {
  let commit
  let getters

  beforeEach(() => {
    commit = jest.fn()
    getters = {}
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  describe('actions', () => {
    describe('search', () => {
      const QUERY = 'test-query'
      const SUGGESTIONS = 'test-suggestions'

      it('should call an API and save the results', async () => {
        IMDB_API.getIMDBSuggestions.mockImplementation(() =>
          Promise.resolve(SUGGESTIONS)
        )

        await store.actions.search({ commit }, QUERY)

        expect(commit).toHaveBeenCalledTimes(3)
        expect(commit).toHaveBeenNthCalledWith(1, 'clearHits')
        expect(commit).toHaveBeenNthCalledWith(2, 'clearError')
        expect(commit).toHaveBeenNthCalledWith(3, 'saveHits', SUGGESTIONS)
      })

      it('should set an error if the query is empty', async () => {
        const invalidQueries = [undefined, false, {}, '']

        let index = 0
        for (let q of invalidQueries) {
          await store.actions.search({ commit }, q)
          index += 1

          expect(commit).toHaveBeenCalledTimes(index)
          expect(commit).toHaveBeenLastCalledWith(
            'saveError',
            'Please, type something into the search bar!'
          )
        }
      })
    })

    describe('searchDetails', () => {
      const TEST_ID = 'test-id'
      const TEST_TITLE = 'test-title'
      const TEST_WIKI_TITLE = 'test-wiki-title'
      const TEST_SUMMARY = { url: 'test-url', extract: 'test-extract' }
      const TEST_ERROR = { url: '', details: 'No information' }

      it('should attempt to fetch the summary for a movie hit', async () => {
        getters.hitMap = {
          [TEST_ID]: {
            title: TEST_TITLE,
          },
        }
        WIKI_API.getWikiSuggestions.mockImplementation(() =>
          Promise.resolve([{ title: TEST_WIKI_TITLE }])
        )
        WIKI_API.getWikiPageSummary.mockImplementation(() =>
          Promise.resolve(TEST_SUMMARY)
        )

        await store.actions.searchDetails({ commit, getters }, TEST_ID)

        expect(WIKI_API.getWikiSuggestions).toHaveBeenCalledWith(TEST_TITLE)
        expect(WIKI_API.getWikiPageSummary).toHaveBeenCalledWith(
          TEST_WIKI_TITLE
        )
        expect(commit).toHaveBeenCalledWith('saveDetails', {
          id: TEST_ID,
          details: TEST_SUMMARY.extract,
          url: TEST_SUMMARY.url,
        })
      })

      it('saves `No information` if the id is missing or malformed', async () => {
        await store.actions.searchDetails({ commit, getters }, '')

        expect(WIKI_API.getWikiSuggestions).toHaveBeenCalledTimes(0)
        expect(WIKI_API.getWikiPageSummary).toHaveBeenCalledTimes(0)
        expect(commit).toHaveBeenCalledWith('saveDetails', {
          id: '',
          ...TEST_ERROR,
        })
      })

      it('saves `No information` if Wiki suggestions are empty', async () => {
        getters.hitMap = {
          [TEST_ID]: {
            title: TEST_TITLE,
          },
        }
        WIKI_API.getWikiSuggestions.mockImplementation(() =>
          Promise.resolve([])
        )

        await store.actions.searchDetails({ commit, getters }, TEST_ID)

        expect(WIKI_API.getWikiPageSummary).toHaveBeenCalledTimes(0)
        expect(commit).toHaveBeenCalledWith('saveDetails', {
          id: TEST_ID,
          ...TEST_ERROR,
        })
      })

      it('saves `No information` if an error happens', async () => {
        getters.hitMap = {
          [TEST_ID]: {
            title: TEST_TITLE,
          },
        }
        WIKI_API.getWikiSuggestions.mockImplementation(() =>
          Promise.reject('Error')
        )

        await store.actions.searchDetails({ commit, getters }, TEST_ID)

        expect(commit).toHaveBeenCalledWith('saveDetails', {
          id: TEST_ID,
          ...TEST_ERROR,
        })
      })
    })
  })

  describe('mutations', () => {
    describe('saveHits', () => {
      it('should save the hits', () => {
        const state = {}
        const NEW_HITS = [1, 2, 3]

        store.mutations.saveHits(state, NEW_HITS)

        expect(state.hits).toBe(NEW_HITS)
      })
    })

    describe('clearHits', () => {
      it('should reset the hit list', () => {
        const state = { hits: [1, 2, 3] }

        store.mutations.clearHits(state)

        expect(state.hits).toEqual([])
      })
    })

    describe('saveError', () => {
      it('should save the error message and set the error flag', () => {
        const state = {}
        const ERROR = 'This Error'

        store.mutations.saveError(state, ERROR)

        expect(state.error).toEqual(true)
        expect(state.errorMessage).toEqual(ERROR)
      })
    })

    describe('clearError', () => {
      it('should reset the error flag and clear the message', () => {
        const state = {
          error: true,
          errorMessage: 'something',
        }

        store.mutations.clearError(state)

        expect(state.error).toEqual(false)
        expect(state.errorMessage).toEqual('')
      })
    })

    describe('saveDetails', () => {
      const TEST_ID = 'test-id'
      const TEST_PAYLOAD = {
        id: TEST_ID,
        details: 'test-details',
        url: 'test-url',
      }
      const state = {
        hits: [{ id: TEST_ID }],
      }

      store.mutations.saveDetails(state, TEST_PAYLOAD)

      expect(state.hits[0].details).toEqual(TEST_PAYLOAD.details)
      expect(state.hits[0].wikiUrl).toEqual(TEST_PAYLOAD.url)
    })
  })

  describe('getters', () => {
    const state = {
      hits: [{ id: 'a', field: 'b' }, { id: 'c', field: 'd' }],
    }

    expect(store.getters.hitMap(state)).toEqual({
      a: {
        id: 'a',
        field: 'b',
      },
      c: {
        id: 'c',
        field: 'd',
      },
    })
  })
})
