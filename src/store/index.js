import Vue from 'vue'
import Vuex from 'vuex'

// import example from './module-example'
import movieSearch from './movie-search'

Vue.use(Vuex)

export default function(/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      movieSearch,
    },
    strict: process.env.DEV,
  })

  return Store
}
